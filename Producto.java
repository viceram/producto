public class Producto
{
    private String codigoDelProducto;
    private String nombreDelProducto;
    private double precioDeCosto;
    private double porcentajeDeGanancia;
    private double iva;
    private double precioDeVenta;

    //---------Setters----------------
    public void setCodigoProducto(String codigoDelProcducto){
        this.codigoDelProducto=codigoDelProcducto;
    }

    public void setNombreProducto(String nombreDelProducto){
        this.nombreDelProducto=nombreDelProducto;
    }

    public void setPrecioDeCosto(double precioDeCosto){
        this.precioDeCosto=precioDeCosto;
    }

    public void setPorcentajeGanancia(double porcentajeDeGanancia){
        this.porcentajeDeGanancia=porcentajeDeGanancia;
    }

    public void setIVA(double iva){
        this.iva=iva;
    }
    //---------Getters----------------
    public String getCodigoProducto(){
        return codigoDelProducto;
    }

    public String getNombreProducto(){
        return nombreDelProducto;
    }

    public double getPrecioDeCosto(){
        return precioDeCosto;
    }

    public double getPorcentajeGanancia(){
        return porcentajeDeGanancia;
    }

    public double getIVA(){
        return iva;
    }
 
    public double getPrecioVenta(){
        return precioDeVenta;
    }
    //-----------------------------------------------
    @Override
    public String toString(){
        return "Codigo: " + codigoDelProducto + ", Nombre: " + nombreDelProducto + 
        ", Precio de costo: " + precioDeCosto + ", Precio de venta: " + precioDeVenta;
    }


    public void calcularPrecioVenta(){
        precioDeVenta=precioDeCosto + (precioDeCosto*porcentajeDeGanancia) + 
        //abajo se aplica el iva
        ((precioDeCosto + (precioDeCosto*porcentajeDeGanancia))*iva);
    }
}