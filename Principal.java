public class Principal {
    public static void main (String[] args){
        Producto producto1=new Producto();

        producto1.setCodigoProducto("9-7895788-9");
        producto1.setNombreProducto("Gaseosa Sprite");
        producto1.setPrecioDeCosto(40.00);
        producto1.setPorcentajeGanancia(0.15);
        producto1.setIVA(0.21);
        producto1.calcularPrecioVenta();

        Producto producto2=new Producto();

        producto2.setCodigoProducto("9-7557711-9");
        producto2.setNombreProducto("Galletas Merengadas");
        producto2.setPrecioDeCosto(32.00);
        producto2.setPorcentajeGanancia(0.15);
        producto2.setIVA(0.21);
        producto2.calcularPrecioVenta();

        System.out.println("Datos del producto1: " + producto1.toString());
        System.out.println("Datos del producto2: " + producto2.toString());

        if(producto1.getPrecioVenta()>producto2.getPrecioVenta()){
            System.out.println("El producto: " + producto1.getNombreProducto() + ", tiene mayor precio de venta");
        }        
        else{
            System.out.println("El producto: " + producto2.getNombreProducto() + ", tiene mayor precio de venta");
        }
    }   
}